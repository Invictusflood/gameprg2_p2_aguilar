// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "GhostTower.h"
#include "BuildNode.h"
#include "Tower.h"
#include "TDGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "ShopItemData.h"
#include "TowerData.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}

void ABuildManager::GetShopItemValues(int Index, FString& TowerName, int& TowerPrice, UTexture2D*& TowerImage)
{
	if (Index < ShopItemList.Num())
	{
		TowerImage = ShopItemList[Index]->TowerImage;
		TowerName = ShopItemList[Index]->TowerData->Name;
		TowerPrice = ShopItemList[Index]->TowerData->Price;
	}
	else
	{
		TowerImage = nullptr;
		TowerName = "OutOfIndex!";
		TowerPrice = 0;
	}
}

void ABuildManager::SelectTower(int Index)
{
	if (Index >= ShopItemList.Num())//Null Guard
	{
		//Index is out of bounds!!!!
		return;
	}

	if (GhostTowerActorRef != nullptr)
	{
		DeselectTower();
	}

	SelectedTowerClassRef = ShopItemList[Index]->TowerData->TowerClassRef;
	GhostTowerActorRef = GetWorld()->SpawnActor<AGhostTower>(ShopItemList[Index]->GhostTowerClassRef);
	SelectedTowerPrice = ShopItemList[Index]->TowerData->Price;
}

void ABuildManager::DeselectTower()
{
	SelectedTowerClassRef = NULL;
	if (GhostTowerActorRef != nullptr)
	{
		GhostTowerActorRef->Destroy();
		GhostTowerActorRef = nullptr;
	}
}

void ABuildManager::UpgradeTowerOfSelectNode()
{
	if (SelectedBuildNode)
	{
		SelectedBuildNode->UpgradeCurrentTower();
		UpdateBuildNodeValues(SelectedBuildNode);
	}
}

void ABuildManager::SellTowerOfSelectNode()
{
	if (SelectedBuildNode)
	{
		SelectedBuildNode->SellCurrentTower();
		CurrentNodeTowerName = " ";
		CurrentNodeTowerValue = 0;
		CurrentNodeTowerUpgradeValue = 0;
		CurrentNodeTurretLevel = 0;
	}
}

void ABuildManager::InteractWithBuildNode(ABuildNode* BuildNode)
{
	if (SelectedTowerClassRef != NULL && BuildNode->CheckIfTurretEmpty())//Purchase
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Test"));
		if (TDGameModeRef->CheckIfEnoughMoney(SelectedTowerPrice))
		{
			TDGameModeRef->RemoveMoney(SelectedTowerPrice);
			BuildNode->SetTurretClassAndSpawn(SelectedTowerClassRef);
			SelectedBuildNode = BuildNode;
			UpdateBuildNodeValues(SelectedBuildNode);
		}

	}
	else if (BuildNode->CheckIfTurretEmpty() == false)//Upgrade or sell
	{
		SelectedBuildNode = BuildNode;
		UpdateBuildNodeValues(SelectedBuildNode);
	}
}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	TDGameModeRef = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildNode::StaticClass(), BuildNodeArray);


	for (AActor * Actor : BuildNodeArray)
	{
		ABuildNode* BuildNode = Cast<ABuildNode>(Actor);
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("success"));
		BuildNode->ClickedEvent.AddDynamic(this, &ABuildManager::OnBuildNodeClicked);
	}

	//SelectTower(0);
	//DeselectTower();
	//SelectTower(0);
}

void ABuildManager::OnBuildNodeClicked(ABuildNode* BuildNode)
{
	if (BuildNode)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("You clicked on a node"));
		InteractWithBuildNode(BuildNode);
	}
}

void ABuildManager::UpdateBuildNodeValues(ABuildNode* BuildNode)
{
	CurrentNodeTowerName = BuildNode->GetCurrentTurretName();
	CurrentNodeTowerValue = BuildNode->GetCurrentTurretValue();
	CurrentNodeTowerUpgradeValue = BuildNode->GetCurrentTurretUpgradeValue();
	CurrentNodeTurretLevel = BuildNode->GetCurrentTurretLevel();
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

