// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "LaserProjectile.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API ALaserProjectile : public AProjectile
{
	GENERATED_BODY()

protected:
	UPROPERTY()
		class UBuff* BuffEffect;

	virtual void DoAction(AEnemy* target) override;

	void DestroySelf() override;

public:

	void SetBuff(class UBuff* Buff);
};
