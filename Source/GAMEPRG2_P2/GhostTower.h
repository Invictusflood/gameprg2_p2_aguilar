// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GhostTower.generated.h"

UCLASS()
class GAMEPRG2_P2_API AGhostTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGhostTower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void FollowCursor();
	UFUNCTION()
		void CheckIfOnBuildNode(FHitResult MouseHitResult);
	UFUNCTION(BlueprintImplementableEvent)
		void ChangeActorMaterial(UMaterial* Material);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, Category = "Tower Range")
		class USphereComponent* SphereRangeComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		APlayerController* PlayerController;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UMaterial* RedMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UMaterial* BlueMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		float SphereRadius;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetRangeSphere(float range);

};
