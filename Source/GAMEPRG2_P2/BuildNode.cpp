// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildNode.h"
#include "Tower.h"
#include "Components/ArrowComponent.h"

// Sets default values
ABuildNode::ABuildNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");
	StaticMesh->SetupAttachment(RootComponent);

	TowerSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	TowerSpawnPoint->SetupAttachment(RootComponent);

}

bool ABuildNode::CheckIfTurretEmpty()
{
	if (TowerActorRef != nullptr)
		return false;
	else
		return true;
}

void ABuildNode::SetTurretClassAndSpawn(TSubclassOf<class ATower> NewTowerClass)
{
	TowerClassRef = NewTowerClass;
	SpawnTurret();
}

FString ABuildNode::GetCurrentTurretName()
{
	if (TowerActorRef != nullptr)
	{
		return TowerActorRef->GetTowerName();
	}
	else
	{
		return TEXT("Error: NULL");
	}
}

int ABuildNode::GetCurrentTurretValue()
{
	if (TowerActorRef != nullptr)
	{
		return TowerActorRef->GetSellValue();
	}
	else
	{
		return 0;
	}
}

int ABuildNode::GetCurrentTurretUpgradeValue()
{
	if (TowerActorRef != nullptr)
	{
		return TowerActorRef->GetUpgradeValue();
	}
	else
		return 0;
}

int ABuildNode::GetCurrentTurretLevel()
{
	if (TowerActorRef != nullptr)
	{
		return TowerActorRef->GetTowerLevel();
	}
	else
		return 0;
}

void ABuildNode::UpgradeCurrentTower()
{
	if (TowerActorRef != nullptr)
	{
		TowerActorRef->UpgradeTower();
	}
}

void ABuildNode::SellCurrentTower()
{
	if (TowerActorRef != nullptr)
	{
		TowerActorRef->SellTower();
		TowerActorRef = nullptr;
	}
}

// Called when the game starts or when spawned
void ABuildNode::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->OnClicked.AddDynamic(this, &ABuildNode::OnBuildNodeClicked);
	//SpawnTurret();
}

// Called every frame
void ABuildNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABuildNode::SpawnTurret()
{
	if (TowerClassRef)
	{
		//TowerActorRef = GetWorld()->SpawnActor<ATower>(TowerClassRef, TowerSpawnPoint->GetComponentTransform());
		TowerActorRef = GetWorld()->SpawnActorDeferred<ATower>(TowerClassRef, TowerSpawnPoint->GetComponentTransform());
		TowerActorRef->InitValues();
		TowerActorRef->FinishSpawning(TowerSpawnPoint->GetComponentTransform());
	}
}

void ABuildNode::RemoveTurret()
{
	if (TowerActorRef)
	{
		TowerActorRef->Destroy();
		TowerActorRef = nullptr;
	}
}

void ABuildNode::OnBuildNodeClicked(UPrimitiveComponent* ClickedComp, FKey ButtonPressed)
{
	ClickedEvent.Broadcast(this);
}

