// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileTower.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>
#include "Enemy.h"
#include "Projectile.h"
#include "Components/SceneComponent.h"
#include "TowerData.h"
#include "MissleProjectile.h"

AProjectileTower::AProjectileTower()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GunScene = CreateDefaultSubobject<USceneComponent>("Gun Scene");
	GunScene->SetupAttachment(BaseMesh);

	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>("Gun Mesh");
	GunMesh->SetupAttachment(GunScene);

	FirePoint->SetupAttachment(GunMesh);
}

void AProjectileTower::SetFireRate(float NewFireRat)
{
	FireRate = NewFireRat;
}

float AProjectileTower::GetFireRate()
{
	return FireRate;
}

void AProjectileTower::UpgradeTower()
{
	Super::UpgradeTower();
}

void AProjectileTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!EnemyTargetRef)
		SetTarget();
	else
		FaceTarget();
}

void AProjectileTower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Added to reflist"));
		//enemy->DeathEvent.AddDynamic(TDGameModeRef, &ATDGameMode::OnEnemyKilled);
		TargetsInRangeArray.Add(enemy);
	}
}

void AProjectileTower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		EnemyTargetRef = nullptr;
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Removed to reflist"));
		TargetsInRangeArray.Remove(enemy);

		SetTarget();
	}
}

void AProjectileTower::SetTarget()
{
	if (TargetsInRangeArray.Num() > 0)
	{
		EnemyTargetRef = Cast<AEnemy>(TargetsInRangeArray[0]);
		FaceTarget();
		DoAction();
	}
}

void AProjectileTower::SetValues()//Add the values with index here
{
	Super::SetValues();
	//DamagePerBullet = TowerData->BaseValue;
	//SetFireRate(TowerData->FireRate);
	if (TowerLevel < LevelDamages.Num())
		DamagePerBullet = LevelDamages[TowerLevel];

	if (TowerLevel < LevelFireRates.Num())
		SetFireRate(LevelFireRates[TowerLevel]);

	if (TowerLevel < LevelSplashRadius.Num())
		ExplodeRadius = LevelSplashRadius[TowerLevel];
}

void AProjectileTower::FaceTarget()
{
	if (EnemyTargetRef)
	{
		FRotator Rot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), EnemyTargetRef->GetActorLocation());
		//FRotator Rot = UKismetMathLibrary::FindLookAtRotation(EnemyTargetRef->GetActorLocation(), this->GetActorLocation());
		GunScene->SetWorldRotation(Rot);
	}
}

void AProjectileTower::DoAction()
{
	//Fire();
	TimerDelegate.BindUFunction(this, FName("Fire"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
}

void AProjectileTower::Fire()
{
	PlayFireSound();
	SpawnProjectile();

	if (EnemyTargetRef)
	{
		TimerDelegate.BindUFunction(this, FName("Fire"));
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
	}
}

void AProjectileTower::SpawnProjectile()
{
	if (ProjectileClassRef)
	{
		//Initialize Damage before actually spawning
		AProjectile* spawnedProjectile = GetWorld()->SpawnActorDeferred<AProjectile>(ProjectileClassRef, FirePoint->GetComponentTransform());
		spawnedProjectile->SetDamage(DamagePerBullet);
		if (AMissleProjectile* Missle = Cast<AMissleProjectile>(spawnedProjectile))
		{
			Missle->SetBlastRadius(ExplodeRadius);
		}
		spawnedProjectile->FinishSpawning(FirePoint->GetComponentTransform());

		if (EnemyTargetRef)
		{
			FRotator Rot = UKismetMathLibrary::FindLookAtRotation(spawnedProjectile->GetActorLocation(), EnemyTargetRef->GetActorLocation());
			spawnedProjectile->SetActorRotation(Rot);
		}
	}
}
