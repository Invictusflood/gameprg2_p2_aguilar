// Fill out your copyright notice in the Description page of Project Settings.


#include "MissleProjectile.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"

AMissleProjectile::AMissleProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereRangeComponent = CreateDefaultSubobject<USphereComponent>("Sphere Radius");
	SphereRangeComponent->SetupAttachment(RootComponent);
	SphereRangeComponent->OnComponentBeginOverlap.AddDynamic(this, &AMissleProjectile::OnBlastSphereHit);
	SphereRangeComponent->Deactivate();
}

void AMissleProjectile::BeginPlay()
{
	Super::BeginPlay();
	SphereRangeComponent->Deactivate();
}

void AMissleProjectile::DoAction(AEnemy* target)
{
	target->TakeDamage(Damage);
	TriggerDeathTimer(ExplodeDuration);
}

void AMissleProjectile::SetBlastRadius(float value)
{
	SphereRangeComponent->SetSphereRadius(value);
}

void AMissleProjectile::OnProjectileHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<AEnemy>(OtherActor))
	{
		//Enable Sphere here
		ProjectileMovement->Deactivate();
		BulletMesh->SetHiddenInGame(true);
		CreateExplosionEffect();
		SphereRangeComponent->Activate();
	}
	//ProjectileMovement->Deactivate();
	//SphereRangeComponent->Activate();
}

void AMissleProjectile::OnBlastSphereHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if(!target->IsFlying)
			DoAction(target);
	}
}
