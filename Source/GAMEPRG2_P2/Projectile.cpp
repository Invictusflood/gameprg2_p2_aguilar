// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"

// Sets default values
AProjectile::AProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Root Scene"));
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->bEditableWhenInherited = true;
	BulletMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnProjectileHit);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("Projectile Movement");
	AddOwnedComponent(ProjectileMovement);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	TriggerDeathTimer(LifeSpan);
}

void AProjectile::OnProjectileHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		DoAction(target);
	}
}

void AProjectile::DoAction(AEnemy* target)
{
	target->TakeDamage(Damage);
	DestroySelf();
}

void AProjectile::DestroySelf()
{
	this->Destroy();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectile::SetDamage(int NewDamage)
{
	Damage = NewDamage;
}

void AProjectile::TriggerDeathTimer(float TimeTillDeath)
{
	TimerDelegate.BindUFunction(this, FName("DestroySelf"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, TimeTillDeath, false);
}

