// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"
#include "Waypoint.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "SpawnData.h"
#include "TDGameMode.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	EnemySpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	EnemySpawnPoint->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	//GetPaths();
	//SpawnEnemy(0, 0);
}

// Called every frame
void AEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

AActor* AEnemySpawner::GetWaypoint(int Index, FString CompareName)
{
	for (FPath Path : PathList)
	{
		if (Path.PathName == CompareName)
		{
			if (Index < Path.Waypoints.Num())
			{
				AActor* WaypointActor = Cast<AActor>(Path.Waypoints[Index]);
				return WaypointActor;
			}
		}
	}
	//When Index is out of range
	return NULL;
}

void AEnemySpawner::SpawnAllEnemiesInWave(int WaveIndex, int Index)
{
	SpawnEnemy(WaveIndex, Index);
	if (Index < SpawnDataRef->WaveList[WaveIndex].Enemies.Num() - 1)
	{
		int NextIndex = Index + 1;
		SpawningTimerDel.BindUFunction(this, FName("SpawnAllEnemiesInWave"), WaveIndex, NextIndex);
		GetWorld()->GetTimerManager().SetTimer(SpawningTimerHandle, SpawningTimerDel, SpawnDataRef->SpawnIntervalTime, false);
	}
}

void AEnemySpawner::SpawnEnemy(int WaveIndex, int Index)
{
	ATDGameMode* TDGameModeRef = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Enemy Spawned"));
	//AEnemy* SpawnedEnemy = GetWorld()->SpawnActor<AEnemy>(SpawnDataRef->WaveList[WaveIndex].Enemies[Index].EnemyRef, GetActorTransform());
	AEnemy* SpawnedEnemy = GetWorld()->SpawnActor<AEnemy>(SpawnDataRef->WaveList[WaveIndex].Enemies[Index].EnemyRef, EnemySpawnPoint->GetComponentTransform());
	SpawnedEnemy->SetupHealth(SpawnDataRef->GetHealthModifier(WaveIndex));
	SpawnedEnemy->SetupKillReward(SpawnDataRef->GetKillRewardModifier(WaveIndex));
	SpawnedEnemy->DeathEvent.AddDynamic(TDGameModeRef, &ATDGameMode::OnEnemyKilled);
	SpawnedEnemy->SetSpawnerAndPathName(this);
	SpawnedEnemy->GetAndMoveToWaypoint();
}

FString AEnemySpawner::GetPathName()
{
	int RandomNum = FMath::RandRange(0, PathList.Num() - 1);
	return PathList[RandomNum].PathName;
}

int AEnemySpawner::GetTotalWaveEnemies(int WaveIndex)
{
	return SpawnDataRef->GetWaveTotalEnemyCount(WaveIndex);
}

void AEnemySpawner::SetTimerTillNextWave(int WaveIndex)
{
	SpawningTimerDel.BindUFunction(this, FName("SpawnAllEnemiesInWave"), WaveIndex, 0);
	GetWorld()->GetTimerManager().SetTimer(SpawningTimerHandle, SpawningTimerDel, SpawnDataRef->WaveIntervalTime, false);
}