// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"
#include "Components/BoxComponent.h"
#include "Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "TDGameMode.h"

// Sets default values
APlayerCore::APlayerCore()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	DamageTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("DamageTrigger"));
	DamageTrigger->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
	DamageTrigger->OnComponentBeginOverlap.AddDynamic(this, &APlayerCore::OnDamageTriggerBeginOverlap);
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerCore::OnDamageTriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		if (!enemy->IsDying)
		{
			ATDGameMode* MyGameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
			MyGameMode->TakeDamage(enemy->EnemyDamage);
			enemy->CoreDeath();
		}
	}
}

void APlayerCore::OnTrigger(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("DWANDIOAWNa"));
}