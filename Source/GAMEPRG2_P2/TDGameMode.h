// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

/**
 *
 */
UCLASS()
class GAMEPRG2_P2_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDGameMode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool isGameDone = false;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealth* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int PlayerHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int PlayerMoney;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int WaveReward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MaxWaves;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int TotalEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int KilledEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> AllSpawners;

	UFUNCTION(BlueprintCallable)
		void Init();
	UFUNCTION()
		void TakeDamage(int damage);
	UFUNCTION()
		void GoToNextWave();
	UFUNCTION()
		void TellSpawnersToSpawnWave(int WaveIndex);
	UFUNCTION(BlueprintCallable)
		void GetTotalEnemyWaveCount();
	UFUNCTION()
		void OnEnemyKilled(int KillValue);

	UFUNCTION()
		void AddMoney(int Value);
	UFUNCTION()
		void RemoveMoney(int Value);
	UFUNCTION()
		bool CheckIfEnoughMoney(int Cost);

	UFUNCTION(BlueprintImplementableEvent)
		void OnPlayerWin();

	UFUNCTION(BlueprintImplementableEvent)
		void OnPlayerLose();
};
