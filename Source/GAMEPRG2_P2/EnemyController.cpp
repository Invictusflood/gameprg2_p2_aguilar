// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"
#include "Enemy.h"

void AEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Test"));
	AEnemy* ControlledEnemy = Cast<AEnemy>(GetPawn());

	if (ControlledEnemy)
	{
		ControlledEnemy->GetAndMoveToWaypoint();
	}
}