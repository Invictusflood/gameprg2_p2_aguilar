// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnData.h"
#include "Enemy.h"

int USpawnData::GetWaveTotalEnemyCount(int WaveIndex)
{
	return WaveList[WaveIndex].Enemies.Num();
}

float USpawnData::GetHealthModifier(int WaveIndex)
{
	return HealthModifier * WaveIndex;
}

float USpawnData::GetKillRewardModifier(int WaveIndex)
{
	return KillRewardModifier * WaveIndex;
}