// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Waypoint.generated.h"

/**
 *
 */
UCLASS()
class GAMEPRG2_P2_API AWaypoint : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	int GetWaypointOrder();
	bool ContainsPathName(FString CompareName);

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		int WaypointOrder;
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TArray<FString> PathNames;
};
