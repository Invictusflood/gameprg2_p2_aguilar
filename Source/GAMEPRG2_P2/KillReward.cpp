// Fill out your copyright notice in the Description page of Project Settings.


#include "KillReward.h"

// Sets default values for this component's properties
UKillReward::UKillReward()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
// Called when the game starts
void UKillReward::BeginPlay()
{
	Super::BeginPlay();
	// ...

}


// Called every frame
void UKillReward::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...s
}

void UKillReward::SetRewardModifier(float Modifier)
{
	RewardModifier += Modifier;
	InitKillReward();
}

void UKillReward::InitKillReward()
{
	CurrentReward = BaseReward * RewardModifier;
}

