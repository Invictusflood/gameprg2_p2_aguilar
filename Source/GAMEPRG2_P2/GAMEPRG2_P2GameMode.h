// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAMEPRG2_P2GameMode.generated.h"

UCLASS(minimalapi)
class AGAMEPRG2_P2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAMEPRG2_P2GameMode();
};



