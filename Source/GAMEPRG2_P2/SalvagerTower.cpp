// Fill out your copyright notice in the Description page of Project Settings.


#include "SalvagerTower.h"
#include "Enemy.h"
#include "SlowBuff.h"
#include "TDGameMode.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

void ASalvagerTower::BeginPlay()
{
	Super::BeginPlay();

	TDGameModeRef = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
}

void ASalvagerTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	currentTime += 1 * DeltaTime;

	if (currentTime >= ReceiveGoldTime)
	{
		GiveCoins(0);
		currentTime = 0;
	}
}

void ASalvagerTower::SetValues()
{
	Super::SetValues();
	if (TowerLevel < LevelGoldGain.Num())
		GenerateMoney = LevelGoldGain[TowerLevel];
}

void ASalvagerTower::GiveCoins(int EnemyReward)
{
	//Use the enemy reward??
	TDGameModeRef->AddMoney(GenerateMoney);
}

void ASalvagerTower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Added to reflist"));
		enemy->DeathEvent.AddDynamic(this, &ASalvagerTower::GiveCoins);
		TargetsInRangeArray.Add(enemy);
	}
}

void ASalvagerTower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Removed to reflist"));
		enemy->DeathEvent.RemoveDynamic(this, &ASalvagerTower::GiveCoins);
		TargetsInRangeArray.Remove(enemy);
	}
}

void ASalvagerTower::DoAction()
{
	////Fire();
	//TimerDelegate.BindUFunction(this, FName("Fire"));
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
}

