// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeath, int, KillReward);

UCLASS()
class GAMEPRG2_P2_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		bool IsFlying;

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
		FOnDeath DeathEvent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealth* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UKillReward* KillRewardComponent;

	UPROPERTY(EditAnywhere)
		FString PathName;

	UPROPERTY(EditAnywhere)
		class AEnemySpawner* Spawner;

	UPROPERTY(EditAnywhere)
		TArray<class UBuff*> CurrentBuffsArray;

	UPROPERTY(EditAnywhere)
		int EnemyDamage = 1;

	UPROPERTY(EditAnywhere)
		bool IsDying = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
		void CreateExplosionEffect();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void TakeDamage(int Damage);
	UFUNCTION()
	void CoreDeath();
	UFUNCTION()
		void Die();
	UFUNCTION()
	void SetSpawnerAndPathName(class AEnemySpawner* SpawnerRef);
	UFUNCTION()
	void GetAndMoveToWaypoint();
	UFUNCTION()
	void SetupHealth(float Modifier);
	UFUNCTION()
		void SetupKillReward(float Modifier);

	//Maybe Delete this??
	UFUNCTION()
		void AddBuffComponent(TSubclassOf<class UBuff> buff);\


	UFUNCTION()
		void TakeBuff(class UBuff* buff);

	UFUNCTION()
		void DestroyBuff(TSubclassOf<class UBuff> BuffClass);

	void RemoveBuffFromList(UBuff* buff);

	UFUNCTION()
		bool IsBuffInBuffList(TSubclassOf<class UBuff> BuffClass);

private:

	//UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
	//	FString PathName;

	//Delete this too
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TSubclassOf<class UBuff> TestBuff;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		int CurrentWaypointIndex;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		AActor* CurrentWaypoint;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		class AWaypoint* CurrentWaypointTest;

};
