// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Health.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPRG2_P2_API UHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealth();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int BaseHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentHealth;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		float HealthModifier = 1.0f;

	UPROPERTY(EditAnywhere)
		bool IsDying = false;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		void SetHealthModifier(float Modifier);
	UFUNCTION()
		void InitHealth();
	UFUNCTION()
		void TakeDamage(int Damage);
	UFUNCTION()
		bool CheckIfDead();

		
};
