// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "AoeTower.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API AAoeTower : public ATower
{
	GENERATED_BODY()
	

protected:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TArray<class UBuff*> CreatedBuffs;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TSubclassOf<class UBuff> BuffClassRef;

	virtual void OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	virtual void DoAction() override;
};
