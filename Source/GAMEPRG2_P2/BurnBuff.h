// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "BurnBuff.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API UBurnBuff : public UBuff
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBurnBuff();

	void SetDuration(float time);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		float BurnDuration;

private:

	FTimerHandle TimerHand;

	FTimerDelegate TimerDel;

	UFUNCTION()
		void TriggerBurnTimer();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetTarget(AActor* target) override;

	void ApplyBuff() override;

	void DestroyBuff() override;

	void SetDamagePerSec(int damage);
};
