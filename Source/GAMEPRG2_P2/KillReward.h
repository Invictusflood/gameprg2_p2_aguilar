// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "KillReward.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPRG2_P2_API UKillReward : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UKillReward();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int BaseReward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentReward;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		float RewardModifier = 1.0f;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		void SetRewardModifier(float Modifier);
	UFUNCTION()
		void InitKillReward();


};
