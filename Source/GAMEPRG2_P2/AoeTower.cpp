// Fill out your copyright notice in the Description page of Project Settings.


#include "AoeTower.h"
#include "Enemy.h"
#include "SlowBuff.h"

void AAoeTower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Added to reflist"));
		TargetsInRangeArray.Add(enemy);

		UBuff* NewBuff = NewObject<UBuff>(enemy, BuffClassRef, "SlowBuff Effect");

		//if (UBurnBuff* BurnBuff = Cast<UBurnBuff>(NewBuff))
		//{
		//	BurnBuff->SetDamagePerSec(DamagePerBullet);
		//}

		this->AddInstanceComponent(NewBuff);
		NewBuff->RegisterComponent();
		NewBuff->SetTarget(enemy);
		enemy->TakeBuff(NewBuff);
		CreatedBuffs.Add(NewBuff);
	}
}

void AAoeTower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Removed to reflist"));
		if (enemy->IsBuffInBuffList(BuffClassRef))
		{
			enemy->DestroyBuff(BuffClassRef);
		}
		TargetsInRangeArray.Remove(enemy);
	}
}

void AAoeTower::DoAction()
{
	////Fire();
	//TimerDelegate.BindUFunction(this, FName("Fire"));
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
}