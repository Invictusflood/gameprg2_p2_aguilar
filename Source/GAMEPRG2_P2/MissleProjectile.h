// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissleProjectile.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API AMissleProjectile : public AProjectile
{
	GENERATED_BODY()

public:
	AMissleProjectile();

	void SetBlastRadius(float value);

protected:
	virtual void BeginPlay() override;

	virtual void DoAction(AEnemy* target) override;

	UPROPERTY(EditAnywhere, Category = "Explode Range")
		class USphereComponent* SphereRangeComponent;

	UPROPERTY(EditAnywhere, Category = "Explode Duration")
		float ExplodeDuration;

	virtual void OnProjectileHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UFUNCTION()
		void OnBlastSphereHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
		void CreateExplosionEffect();
};
