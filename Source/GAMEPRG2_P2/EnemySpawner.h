// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

USTRUCT(BlueprintType)
struct FPath
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FString PathName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<class AWaypoint*> Waypoints;
};

UCLASS()
class GAMEPRG2_P2_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	FTimerDelegate SpawningTimerDel;
	FTimerHandle SpawningTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Spawn Location")
	class UArrowComponent* EnemySpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<FPath> PathList;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Enemy Class Ref")
	//TArray<TSubclassOf<class AEnemy>> EnemyClassRefArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "SpawnData")
	class USpawnData* SpawnDataRef;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	AActor* GetWaypoint(int Index, FString CompareName);


	UFUNCTION()
		void SpawnAllEnemiesInWave(int WaveIndex, int Index);

	UFUNCTION()
		void SpawnEnemy(int WaveIndex, int Index);

	UFUNCTION()
	FString GetPathName();

	UFUNCTION()
	int GetTotalWaveEnemies(int WaveIndex);

	UFUNCTION()
	void SetTimerTillNextWave(int WaveIndex);
};
