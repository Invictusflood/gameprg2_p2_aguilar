// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class GAMEPRG2_P2_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectile();

	UFUNCTION()
		void SetDamage(int NewDamage);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void OnProjectileHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void DoAction(AEnemy* target);

	UFUNCTION()
		virtual void DestroySelf();

	void TriggerDeathTimer(float TimeTillDeath);

	FTimerHandle TimerHandle;

	FTimerDelegate TimerDelegate;

	UPROPERTY(EditAnywhere, Category = "Base Mesh")
		class UStaticMeshComponent* BulletMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		float LifeSpan;

	UPROPERTY()
		int Damage;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
