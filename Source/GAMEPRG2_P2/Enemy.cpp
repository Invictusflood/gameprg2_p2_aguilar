// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "EnemyController.h"
#include "EnemySpawner.h"
#include "Health.h"
#include "KillReward.h"
#include "Buff.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealth>("Health Component");
	AddOwnedComponent(HealthComponent);

	KillRewardComponent = CreateDefaultSubobject<UKillReward>("Kill Reward Component");
	AddOwnedComponent(KillRewardComponent);

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	//InitHealth();

	//if(TestBuff)
	//	AddBuffComponent(TestBuff);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::GetAndMoveToWaypoint()
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Test"));
	CurrentWaypoint = Spawner->GetWaypoint(CurrentWaypointIndex, PathName); //Returns NULL if index out of bounds
	CurrentWaypointIndex++;

	if (CurrentWaypoint == NULL)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("CurrentWaypoint is NULL"));
		return; //Return here so you don't have to run code below
	}

	AEnemyController* EnemyController = Cast<AEnemyController>(GetController());

	if (EnemyController && CurrentWaypoint)
	{
		EnemyController->MoveToActor(CurrentWaypoint);
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Test"));
	}
}

void AEnemy::SetupHealth(float Modifier)
{
	if(HealthComponent)
		HealthComponent->SetHealthModifier(Modifier);
}

void AEnemy::SetupKillReward(float Modifier)
{
	if (KillRewardComponent)
		KillRewardComponent->SetRewardModifier(Modifier);
}

void AEnemy::AddBuffComponent(TSubclassOf<class UBuff> buff)
{
	UBuff* NewBuff = NewObject<UBuff>(this, buff, "Effect");
	AddInstanceComponent(NewBuff);
	NewBuff->RegisterComponent();

	NewBuff->SetTarget(this);
}

void AEnemy::TakeDamage(int Damage)
{
	if (HealthComponent)
	{
		HealthComponent->TakeDamage(Damage);

		if (HealthComponent->CheckIfDead())
		{
			//Get Money for player
			DeathEvent.Broadcast(KillRewardComponent->CurrentReward);
			Die();
		}
	}
}

void AEnemy::CoreDeath()
{
	if (!IsDying)
	{
		IsDying = true;
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Enemy Hit Core"));
		DeathEvent.Broadcast(0);
		Die();
	}
}

void AEnemy::Die()
{
	CreateExplosionEffect();
	Destroy();
}

void AEnemy::SetSpawnerAndPathName(AEnemySpawner* SpawnerRef)
{
	Spawner = SpawnerRef;
	PathName = Spawner->GetPathName();
}

void AEnemy::TakeBuff(class UBuff* buff)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Enemy: apply buff called!"));

	UBuff* NewBuff = Cast<UBuff>(buff);

	CurrentBuffsArray.Add(NewBuff);

	NewBuff->ApplyBuff();
}

void AEnemy::DestroyBuff(TSubclassOf<class UBuff> BuffClass)
{
	//if (curBuff)
	//	curBuff->RemoveBuff();

	for (UBuff* buff : CurrentBuffsArray)
	{
		if (buff->GetClass() == BuffClass)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Enemy: buff removed"));
			buff->DestroyBuff();
			//CurrentBuffsArray.Remove(buff);
		}
	}
}
void AEnemy::RemoveBuffFromList(class UBuff* buff)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Enemy: buff removed"));
	CurrentBuffsArray.Remove(buff);
}


bool AEnemy::IsBuffInBuffList(TSubclassOf<class UBuff> BuffClass)
{
	for (UBuff* buff : CurrentBuffsArray)
	{
		if (buff->GetClass() == BuffClass)
		{
			return true;
		}
	}
	return false;
}

