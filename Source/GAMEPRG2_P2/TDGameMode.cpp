// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"
#include "EnemySpawner.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "Health.h"

ATDGameMode::ATDGameMode()
{
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealth>("Health Component");
	//UActorComponent* test = Cast<UActorComponent>(HealthComponent);
	AddOwnedComponent(HealthComponent);
}


// Called when the game starts or when spawned
void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Test"));
	Init();
	GetTotalEnemyWaveCount();
	TellSpawnersToSpawnWave(CurrentWave - 1);
}

void ATDGameMode::Init()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemySpawner::StaticClass(), AllSpawners);
}

void ATDGameMode::TakeDamage(int damage)
{
	PlayerHealth -= damage;
	if (PlayerHealth <= 0 && !isGameDone)
	{
		isGameDone = true;
		OnPlayerLose();
	}
}

void ATDGameMode::GoToNextWave()
{
	if (CurrentWave < MaxWaves)
	{
		CurrentWave++;
		GetTotalEnemyWaveCount();
		TellSpawnersToSpawnWave(CurrentWave - 1);
		AddMoney(WaveReward);
	}
	else if (CurrentWave >= MaxWaves && !isGameDone)
	{
		isGameDone = true;
		OnPlayerWin();
	}
}

void ATDGameMode::TellSpawnersToSpawnWave(int WaveIndex)
{
	for (AActor* SpawnerActor : AllSpawners)
	{
		AEnemySpawner* EnemySpawner = Cast<AEnemySpawner>(SpawnerActor);
		if (EnemySpawner)
		{
			EnemySpawner->SetTimerTillNextWave(WaveIndex);
		}
	}
}

void ATDGameMode::GetTotalEnemyWaveCount()
{
	TotalEnemies = 0;
	KilledEnemies = 0;

	int EnemyCount = 0;

	for (AActor* SpawnerActor : AllSpawners)
	{
		AEnemySpawner* EnemySpawner = Cast<AEnemySpawner>(SpawnerActor);
		if (EnemySpawner)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Test"));
			EnemyCount += EnemySpawner->GetTotalWaveEnemies(CurrentWave - 1);
		}
	}

	TotalEnemies = EnemyCount;
}

void ATDGameMode::OnEnemyKilled(int KillValue)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Killed Enemy add"));
	KilledEnemies++;

	if (KilledEnemies >= TotalEnemies)
	{
		GoToNextWave();
	}

	if (KillValue > 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Money Added"));
		AddMoney(KillValue);
	}
}

void ATDGameMode::AddMoney(int Value)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Money Supposedly added"));
	PlayerMoney += Value;
}

void ATDGameMode::RemoveMoney(int Value)
{
	PlayerMoney -= Value;
}

bool ATDGameMode::CheckIfEnoughMoney(int Cost)
{
	if (Cost <= PlayerMoney)
		return true;
	else
		return false;
}
