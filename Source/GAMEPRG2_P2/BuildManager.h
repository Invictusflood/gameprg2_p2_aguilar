// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class GAMEPRG2_P2_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

	UFUNCTION(BlueprintCallable)
	void GetShopItemValues(int Index, FString& TowerName, int& TowerPrice, UTexture2D*& TowerImage);

	UFUNCTION(BlueprintCallable)
	void SelectTower(int Index);

	UFUNCTION(BlueprintCallable)
		void DeselectTower();

	UFUNCTION(BlueprintCallable)
		void UpgradeTowerOfSelectNode();

	UFUNCTION(BlueprintCallable)
		void SellTowerOfSelectNode();

	UFUNCTION()
		void InteractWithBuildNode(ABuildNode* BuildNode);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UShopItemData*> ShopItemList;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<AActor*> BuildNodeArray;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString CurrentNodeTowerName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int CurrentNodeTowerValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int CurrentNodeTowerUpgradeValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int CurrentNodeTurretLevel;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class ABuildNode* SelectedBuildNode;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnBuildNodeClicked(ABuildNode* BuildNode);

	UFUNCTION()
		void UpdateBuildNodeValues(ABuildNode* BuildNode);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		class ATDGameMode* TDGameModeRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		class AGhostTower* GhostTowerActorRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class ATower> SelectedTowerClassRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		int SelectedTowerPrice;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
