// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNodeClicked, ABuildNode*, BuildNodeRef);

UCLASS()
class GAMEPRG2_P2_API ABuildNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildNode();

	UFUNCTION()
		bool CheckIfTurretEmpty();
	UFUNCTION()
		void SetTurretClassAndSpawn(TSubclassOf<class ATower> TowerClass);
	UFUNCTION()
		FString GetCurrentTurretName();

	UFUNCTION()
		int GetCurrentTurretValue();

	UFUNCTION()
		int GetCurrentTurretUpgradeValue();

	int GetCurrentTurretLevel();

	UFUNCTION()
		void UpgradeCurrentTower();

	UFUNCTION()
		void SellCurrentTower();

	UPROPERTY(EditAnywhere, Category = "Static Mesh")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
		FOnNodeClicked ClickedEvent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Spawn Location")
		class UArrowComponent* TowerSpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Tower Refs")
		TSubclassOf<class ATower> TowerClassRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Tower Refs")
		class ATower* TowerActorRef;

	UFUNCTION()
		void SpawnTurret();
	UFUNCTION()
		void RemoveTurret();
	UFUNCTION()
		void OnBuildNodeClicked(UPrimitiveComponent* ClickedComp, FKey ButtonPressed);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
