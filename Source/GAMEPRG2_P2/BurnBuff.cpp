// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnBuff.h"
#include "Enemy.h"

UBurnBuff::UBurnBuff()
{

}

void UBurnBuff::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("BurnBuff Was Created!"));


	TimerDel.BindUFunction(this, FName("TriggerBurnTimer"));
}

void UBurnBuff::TriggerBurnTimer()
{
	if (BuffTarget)
	{
		if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
			enemy->TakeDamage(BuffEffectValue);

		GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 1.f, false);
	}
	else
		DestroyBuff();//Destroy buff if target has also been destroyed
}

void UBurnBuff::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	curTime += 1 * DeltaTime;

	if (curTime >= duration)
		DestroyBuff();
}

void UBurnBuff::SetTarget(AActor* target)
{
	Super::SetTarget(target);
}

void UBurnBuff::ApplyBuff()
{
	if (BuffTarget)
	{
		TriggerBurnTimer();

		curTime = 0;
	}
}

void UBurnBuff::DestroyBuff()
{
	Super::DestroyBuff();
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Buff Destroyed"));
}

void UBurnBuff::SetDamagePerSec(int damage)
{
	BuffEffectValue = damage;
}

void UBurnBuff::SetDuration(float time)
{
	duration = time;
}
