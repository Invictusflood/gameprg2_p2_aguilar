// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ShopItemData.generated.h"

/**
 *
 */
UCLASS()
class GAMEPRG2_P2_API UShopItemData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UTexture2D* TowerImage;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//	TSubclassOf<class ATower> TowerClassRef;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AGhostTower> GhostTowerClassRef;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//	FString Name;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//	int Price;
};
