// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class GAMEPRG2_P2_API ATower : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATower();

	UFUNCTION()
		FString GetTowerName();

	int GetSellValue();

	int GetUpgradeValue();

	int GetTowerLevel();

	float GetRange();

	UFUNCTION()
		void TakeBuff(class UBuff* buff);

	UFUNCTION()
		void DestroyBuff(TSubclassOf<class UBuff> BuffClass);

	UFUNCTION()
		bool IsBuffInBuffList(TSubclassOf<class UBuff> BuffClass);

	UFUNCTION(BlueprintCallable)
		virtual void UpgradeTower();

	void InitValues();

	UFUNCTION(BlueprintCallable)
		void SellTower();

	UPROPERTY(EditAnywhere)
		TArray<class UBuff*> CurrentBuffsArray;

	UPROPERTY(EditAnywhere)
		int BuyValue;

	UPROPERTY(EditAnywhere)
		int UpgradeValue;

	UPROPERTY(EditAnywhere)
		int SellValue;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
		void ChangeActorMaterial(UMaterial* Material);

	UFUNCTION()
		virtual void OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void DoAction();

	UFUNCTION()
		virtual void SetValues();

	UFUNCTION()
		void AddToPriceValues();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		int TowerLevel = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		int MaxTowerLevel = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<UMaterial*> LevelMaterials;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UMaterial* CurrentMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		float Radius;

	UPROPERTY(EditAnywhere, Category = "Tower Range", BlueprintReadWrite)
		class USphereComponent* SphereRangeComponent;

	UPROPERTY(EditAnywhere, Category = "Base Mesh", BlueprintReadWrite)
		class UStaticMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, Category = "Fire Point")
		class UArrowComponent* FirePoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Target References")
		TArray<AActor*> TargetsInRangeArray;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
