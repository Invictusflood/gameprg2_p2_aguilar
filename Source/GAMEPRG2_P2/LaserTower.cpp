// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>
#include "Enemy.h"
#include "Projectile.h"
#include "Components/SceneComponent.h"
#include "TowerData.h"
#include "LaserProjectile.h"
#include "Buff.h"
#include "BurnBuff.h"

ALaserTower::ALaserTower()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ALaserTower::SetValues()
{
	Super::SetValues();
	if(TowerLevel < LevelDamages.Num())
		DamagePerBullet = LevelDamages[TowerLevel];

	if (TowerLevel < LevelBurnDuration.Num())
		BurnDuration = LevelBurnDuration[TowerLevel];
}

void ALaserTower::SpawnProjectile()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Spawn Proj"));
	if (ProjectileClassRef)
	{
		if (EnemyTargetRef)
		{
			UBuff* NewBuff = NewObject<UBuff>(EnemyTargetRef, BuffClassRef, "BurnBuff Effect");

			if (UBurnBuff* BurnBuff = Cast<UBurnBuff>(NewBuff))
			{
				BurnBuff->SetDamagePerSec(DamagePerBullet);
				BurnBuff->SetDuration(BurnDuration);
			}

			this->AddInstanceComponent(NewBuff);
			NewBuff->RegisterComponent();

			AddBuffToCreatedBuffs(NewBuff);

			//Initialize Damage before actually spawning
			AProjectile* spawnedProjectile = GetWorld()->SpawnActorDeferred<AProjectile>(ProjectileClassRef, FirePoint->GetComponentTransform());
			if (ALaserProjectile* Laser = Cast<ALaserProjectile>(spawnedProjectile))
			{
				Laser->SetBuff(NewBuff);
			}

			spawnedProjectile->FinishSpawning(FirePoint->GetComponentTransform());
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Laser Spawned"));

			FRotator Rot = UKismetMathLibrary::FindLookAtRotation(spawnedProjectile->GetActorLocation(), EnemyTargetRef->GetActorLocation());
			spawnedProjectile->SetActorRotation(Rot);
		}
	}
}

void ALaserTower::AddBuffToCreatedBuffs(UBuff* NewBuff)
{
	for (UBuff* buff : CreatedBuffs)
	{
		if (buff == nullptr)
			CreatedBuffs.Remove(buff);
	}

	CreatedBuffs.Add(NewBuff);
}

void ALaserTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (!EnemyTargetRef)
	//	SetTarget();
	//else
	//	FaceTarget();

	AEnemy* Enemy = Cast<AEnemy>(EnemyTargetRef);
	if (Enemy)
	{
		if (Enemy->IsBuffInBuffList(BuffClassRef) == true)
			EnemyTargetRef = nullptr;
	}
}

void ALaserTower::SetTarget()
{
	if (TargetsInRangeArray.Num() > 0)
	{
		for (AActor* EnemyActor : TargetsInRangeArray)
		{
			AEnemy* Enemy = Cast<AEnemy>(EnemyActor);
			if (Enemy->IsBuffInBuffList(BuffClassRef) == false)
			{
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Override"));
				EnemyTargetRef = Enemy;
				FaceTarget();
				DoAction();
				return; //Exit Function
			}
		}
	}
}

