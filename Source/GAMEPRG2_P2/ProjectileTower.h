// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "ProjectileTower.generated.h"

/**
 *
 */

UCLASS()
class GAMEPRG2_P2_API AProjectileTower : public ATower
{
	GENERATED_BODY()

public:
	AProjectileTower();

	void SetFireRate(float NewFireRate);

	float GetFireRate();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		float ExplodeRadius;

	virtual void UpgradeTower() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
protected:

	virtual void OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UFUNCTION(BlueprintImplementableEvent)
		void PlayFireSound();

	UFUNCTION()
		virtual void SetTarget();

	virtual void SetValues() override;

	FTimerHandle TimerHandle;

	FTimerDelegate TimerDelegate;

	UPROPERTY(EditAnywhere, Category = "Gun Scene")
		class USceneComponent* GunScene;

	UPROPERTY(EditAnywhere, Category = "Gun Mesh", BlueprintReadWrite)
		class UStaticMeshComponent* GunMesh;

	UPROPERTY(EditAnywhere, Category = "Target References")
		AActor* EnemyTargetRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> ProjectileClassRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		int DamagePerBullet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<int> LevelDamages;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<float> LevelFireRates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<float> LevelSplashRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<float> LevelBurnDuration;

	UFUNCTION()
		void FaceTarget();

	virtual void DoAction() override;

	UFUNCTION()
		virtual void Fire();

	UFUNCTION()
		virtual void SpawnProjectile();

};
