// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGeneratorTower.h"
#include "Enemy.h"
#include "FireRateBuff.h"
#include "TowerData.h"

void APowerGeneratorTower::UpgradeTower()
{
	Super::UpgradeTower();

}

void APowerGeneratorTower::SetValues()
{
	Super::SetValues();
	if (TowerLevel < LevelFireRateBoost.Num())
	{
		FireRateModifier = LevelFireRateBoost[TowerLevel];
	}
	ResetBuffs();
}

void APowerGeneratorTower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATower* tower = Cast<ATower>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Turret Added to reflist"));
		TargetsInRangeArray.Add(tower);

		UBuff* NewBuff = NewObject<UBuff>(tower, BuffClassRef, "FireRateBuff Effect");

		if (UFireRateBuff* FireRateBuff = Cast<UFireRateBuff>(NewBuff))
		{
			FireRateBuff->SetBuffEffectValue(FireRateModifier);
		}

		this->AddInstanceComponent(NewBuff);
		NewBuff->RegisterComponent();
		NewBuff->SetTarget(tower);
		tower->TakeBuff(NewBuff);
		CreatedBuffs.Add(NewBuff);
	}
}

void APowerGeneratorTower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (ATower* tower = Cast<ATower>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Turret Removed to reflist"));
		if (tower->IsBuffInBuffList(BuffClassRef))
		{
			tower->DestroyBuff(BuffClassRef);
		}
		TargetsInRangeArray.Remove(tower);
	}
}

void APowerGeneratorTower::ResetBuffs()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Buff Reset"));
	for (AActor* actor : TargetsInRangeArray)
	{
		if (ATower* tower = Cast<ATower>(actor))
		{
			if (tower->IsBuffInBuffList(BuffClassRef))
			{
				tower->DestroyBuff(BuffClassRef);

				UBuff* NewBuff = NewObject<UBuff>(tower, BuffClassRef, "FireRateBuff Effect");

				if (UFireRateBuff* FireRateBuff = Cast<UFireRateBuff>(NewBuff))
				{
					FireRateBuff->SetBuffEffectValue(FireRateModifier);
				}

				this->AddInstanceComponent(NewBuff);
				NewBuff->RegisterComponent();
				NewBuff->SetTarget(tower);
				tower->TakeBuff(NewBuff);
				CreatedBuffs.Add(NewBuff);
			}
		}
	}
}

void APowerGeneratorTower::DoAction()
{
	////Fire();
	//TimerDelegate.BindUFunction(this, FName("Fire"));
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
}
