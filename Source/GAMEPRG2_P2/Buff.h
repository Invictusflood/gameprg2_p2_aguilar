// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Buff.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPRG2_P2_API UBuff : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBuff();

	UPROPERTY()
		AActor* BuffTarget;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		float duration;

	UPROPERTY(EditAnywhere)
		float BuffEffectValue;

	UPROPERTY(EditAnywhere)
		float curTime;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void ApplyBuff();

	virtual void DestroyBuff();

	virtual void SetTarget(AActor* target);
};
