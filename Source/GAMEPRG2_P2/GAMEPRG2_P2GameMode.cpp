// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAMEPRG2_P2GameMode.h"
#include "GAMEPRG2_P2Character.h"
#include "UObject/ConstructorHelpers.h"

AGAMEPRG2_P2GameMode::AGAMEPRG2_P2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
