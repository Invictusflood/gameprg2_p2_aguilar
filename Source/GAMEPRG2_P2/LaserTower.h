// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileTower.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API ALaserTower : public AProjectileTower
{
	GENERATED_BODY()

public:
	ALaserTower();

	virtual void Tick(float DeltaTime) override;
protected:

	UPROPERTY()
		TArray<class UBuff*> CreatedBuffs;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TSubclassOf<class UBuff> BuffClassRef;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
	float BurnDuration;

	virtual void SetValues() override;

	void SpawnProjectile() override;

	void AddBuffToCreatedBuffs(UBuff* NewBuff);

	void SetTarget() override;
};
