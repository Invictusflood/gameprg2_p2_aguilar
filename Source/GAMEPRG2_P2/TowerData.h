// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API UTowerData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATower> TowerClassRef;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int Price;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float Range;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BaseValue;//This can be used for slow, firerate boosting, enemy reward boosting

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float UpgradeModifier;
};
