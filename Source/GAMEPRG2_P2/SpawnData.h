// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpawnData.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FEnemyType
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AEnemy> EnemyRef;
};

USTRUCT(BlueprintType)
struct FWave
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FEnemyType> Enemies;
};

UCLASS()
class GAMEPRG2_P2_API USpawnData : public UDataAsset
{
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float SpawnIntervalTime;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float WaveIntervalTime;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float HealthModifier = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float KillRewardModifier = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FWave> WaveList;

	UFUNCTION()
		int GetWaveTotalEnemyCount(int WaveIndex);

	UFUNCTION()
		float GetHealthModifier(int WaveIndex);
	UFUNCTION()
		float GetKillRewardModifier(int WaveIndex);
};
