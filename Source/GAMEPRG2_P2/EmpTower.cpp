// Fill out your copyright notice in the Description page of Project Settings.


#include "EmpTower.h"
#include "Enemy.h"
#include "SlowBuff.h"
#include "Components/SphereComponent.h"

void AEmpTower::SetValues()
{
	Super::SetValues();
	if (TowerLevel < LevelSlowSpeed.Num())
		SlowSpeed = LevelSlowSpeed[TowerLevel];

	if (TowerLevel < LevelRadius.Num())
		SphereRangeComponent->SetSphereRadius(LevelRadius[TowerLevel]);
}

void AEmpTower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Added to reflist"));
		TargetsInRangeArray.Add(enemy);

		UBuff* NewBuff = NewObject<UBuff>(enemy, BuffClassRef, "SlowBuff Effect");

		if (USlowBuff* SlowBuff = Cast<USlowBuff>(NewBuff))
		{
			SlowBuff->SetSlowSpeed(SlowSpeed);
		}

		this->AddInstanceComponent(NewBuff);
		NewBuff->RegisterComponent();
		NewBuff->SetTarget(enemy);
		enemy->TakeBuff(NewBuff);
		CreatedBuffs.Add(NewBuff);
	}
}

void AEmpTower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Removed to reflist"));
		if (enemy->IsBuffInBuffList(BuffClassRef))
		{
			enemy->DestroyBuff(BuffClassRef);
		}
		TargetsInRangeArray.Remove(enemy);
	}
}

void AEmpTower::DoAction()
{
	////Fire();
	//TimerDelegate.BindUFunction(this, FName("Fire"));
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, FireRate, false);
}
