// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "SlowBuff.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API USlowBuff : public UBuff
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USlowBuff();

	void SetSlowSpeed(float NewSpeed);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
		float SlowSpeed;
private:

	FTimerHandle TimerHand;

	FTimerDelegate TimerDel;

	UFUNCTION()
		void TriggerBurnTimer();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetTarget(AActor* target) override;

	void ApplyBuff() override;

	void DestroyBuff() override;

	void SetDamagePerSec(int damage);
};
