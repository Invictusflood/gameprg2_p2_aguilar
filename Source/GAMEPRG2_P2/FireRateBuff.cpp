// Fill out your copyright notice in the Description page of Project Settings.


#include "FireRateBuff.h"
#include "ProjectileTower.h"
#include "GameFramework/CharacterMovementComponent.h"

UFireRateBuff::UFireRateBuff()
{

}

void UFireRateBuff::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("BurnBuff Was Created!"));

	ApplyBuff();

	//BuffEffectValue = 5;
	//duration = 5;

	//TimerDel.BindUFunction(this, FName("TriggerBurnTimer"));
}

void UFireRateBuff::TriggerBurnTimer()
{
	//if (BuffTarget)
	//{
	//	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
	//		enemy->TakeDamage(BuffEffectValue);

	//	GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 1.f, false);
	//}
	//else
	//	RemoveBuff();//Destroy buff if target has also been destroyed
}

void UFireRateBuff::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	//curTime += 1 * DeltaTime;

	//if (curTime >= duration)
	//	RemoveBuff();
}

void UFireRateBuff::SetTarget(AActor* target)
{
	Super::SetTarget(target);

	//if (AProjectileTower* tower = Cast<AProjectileTower>(BuffTarget))
	//{
	//	TargetOriginalFireRate = tower->GetFireRate();
	//}
}

void UFireRateBuff::ApplyBuff()
{
	if (AProjectileTower* tower = Cast<AProjectileTower>(BuffTarget))
	{
		tower->SetFireRate(tower->FireRate * BuffEffectValue);
		curTime = 0;
	}
}

void UFireRateBuff::DestroyBuff()
{
	if (AProjectileTower* tower = Cast<AProjectileTower>(BuffTarget))
	{
		tower->SetFireRate(tower->FireRate / BuffEffectValue);
	}

	Super::DestroyBuff();
}

void UFireRateBuff::SetBuffEffectValue(float value)
{
	BuffEffectValue = value;
}
