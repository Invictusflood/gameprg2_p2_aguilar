// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();
	// ...
	
}


// Called every frame
void UHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealth::SetHealthModifier(float Modifier)
{
	HealthModifier += Modifier;
	InitHealth();
}

void UHealth::InitHealth()
{
	CurrentHealth = BaseHealth * HealthModifier;
}

void UHealth::TakeDamage(int Damage)
{
	CurrentHealth -= Damage;
}

bool UHealth::CheckIfDead()
{
	if (CurrentHealth <= 0)
		return true;
	else
		return false;
}
