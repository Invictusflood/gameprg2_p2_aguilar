// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"
#include "Buff.h"

void ALaserProjectile::DoAction(AEnemy* target)
{
	BuffEffect->SetTarget(target);

	target->TakeBuff(BuffEffect);

	DestroySelf();
}

void ALaserProjectile::SetBuff(UBuff* Buff)
{
	BuffEffect = Buff;
}

void ALaserProjectile::DestroySelf()
{
	if (BuffEffect->BuffTarget == nullptr)//If buff never got a target destroy
	{
		BuffEffect->DestroyBuff();
	}

	this->Destroy();
}
