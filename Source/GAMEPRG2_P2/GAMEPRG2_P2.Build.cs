// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GAMEPRG2_P2 : ModuleRules
{
	public GAMEPRG2_P2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
