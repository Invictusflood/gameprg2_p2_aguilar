// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "TowerData.h"
#include "Buff.h"
#include "TDGameMode.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
ATower::ATower()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	SphereRangeComponent = CreateDefaultSubobject<USphereComponent>("Sphere Radius");
	SphereRangeComponent->SetupAttachment(RootComponent);

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("Base Mesh");
	BaseMesh->SetupAttachment(RootComponent);

	FirePoint = CreateDefaultSubobject<UArrowComponent>("Fire Point");
	FirePoint->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	InitValues();
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATower::OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: Enemy Added to reflist"));
	//	EnemiesInRangeArray.Add(enemy);
	//}
}

void ATower::OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
}

void ATower::DoAction()
{
}

void ATower::SetValues()
{
	if (TowerLevel < LevelMaterials.Num())
	{
		CurrentMaterial = LevelMaterials[TowerLevel];
		ChangeActorMaterial(LevelMaterials[TowerLevel]);
	}
}

void ATower::AddToPriceValues()
{
	BuyValue += UpgradeValue;
	UpgradeValue = BuyValue * 0.25f; //1/4 of full price
	SellValue = (BuyValue / 2.0f);
}

void ATower::TakeBuff(class UBuff* buff)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Turret: apply buff called!"));

	UBuff* NewBuff = Cast<UBuff>(buff);

	CurrentBuffsArray.Add(NewBuff);

	NewBuff->ApplyBuff();
}

void ATower::DestroyBuff(TSubclassOf<class UBuff> BuffClass)
{
	//if (curBuff)
	//	curBuff->RemoveBuff();

	for (UBuff* buff : CurrentBuffsArray)
	{
		if (buff->GetClass() == BuffClass)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Turret: buff removed"));
			buff->DestroyBuff();
			CurrentBuffsArray.Remove(buff);
		}
	}
}

bool ATower::IsBuffInBuffList(TSubclassOf<class UBuff> BuffClass)
{
	for (UBuff* buff : CurrentBuffsArray)
	{
		if (buff->GetClass() == BuffClass)
		{
			return true;
		}
	}
	return false;
}

void ATower::UpgradeTower()
{
	if (TowerLevel < MaxTowerLevel)
	{
		ATDGameMode* TDGameModeRef = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (TDGameModeRef->CheckIfEnoughMoney(UpgradeValue))
		{
			TDGameModeRef->RemoveMoney(UpgradeValue);
			AddToPriceValues();//Set Price Values
			TowerLevel++;
			SetValues();
		}
	}
}

void ATower::InitValues()
{
	SphereRangeComponent->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnRangeOverlapBegin);
	SphereRangeComponent->OnComponentEndOverlap.AddDynamic(this, &ATower::OnRangeOverlapEnd);
	//SphereRangeComponent->SetHiddenInGame(true);
	SphereRangeComponent->SetSphereRadius(TowerData->Range);
	SetValues();

	BuyValue = TowerData->Price;
	UpgradeValue = BuyValue * 0.25f; //1/4 of full price
	SellValue = (BuyValue / 2.0f);
}

void ATower::SellTower()
{
	ATDGameMode* TDGameModeRef = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	TDGameModeRef->AddMoney(SellValue);
	this->Destroy();
}

FString ATower::GetTowerName()
{
	return TowerData->Name;
}

int ATower::GetSellValue()
{
	return SellValue;
}

int ATower::GetUpgradeValue()
{
	return UpgradeValue;
}

int ATower::GetTowerLevel()
{
	return TowerLevel;
}

float ATower::GetRange()
{
	return Radius;
}

