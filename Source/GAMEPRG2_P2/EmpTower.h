// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "EmpTower.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPRG2_P2_API AEmpTower : public ATower
{
	GENERATED_BODY()


protected:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TArray<class UBuff*> CreatedBuffs;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		TSubclassOf<class UBuff> BuffClassRef;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		float SlowSpeed;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAcess = "true"))
		float RangeRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<float> LevelSlowSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Upgrade Values")
		TArray<float> LevelRadius;

	virtual void SetValues() override;

	virtual void OnRangeOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnRangeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	virtual void DoAction() override;
};
