// Fill out your copyright notice in the Description page of Project Settings.


#include "GhostTower.h"
#include "BuildNode.h"
#include "Components/SphereComponent.h"
#include "TowerData.h"

// Sets default values
AGhostTower::AGhostTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	SphereRangeComponent = CreateDefaultSubobject<USphereComponent>("Tower Range");
	SphereRangeComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AGhostTower::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = GetWorld()->GetFirstPlayerController();

	if (TowerData != nullptr)
	{
		SphereRangeComponent->SetSphereRadius(TowerData->Range);
	}
}

// Called every frame
void AGhostTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FollowCursor();
}

void AGhostTower::SetRangeSphere(float range)
{
	SphereRangeComponent->SetSphereRadius(range);
}

void AGhostTower::FollowCursor()
{
	FHitResult MouseHitResult;
	//APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, MouseHitResult);
	SetActorLocation(MouseHitResult.Location);

	CheckIfOnBuildNode(MouseHitResult);
}

void AGhostTower::CheckIfOnBuildNode(FHitResult MouseHitResult)
{
	ABuildNode* BuildNodeActor = Cast<ABuildNode>(MouseHitResult.GetActor());
	if (BuildNodeActor)
	{
		if (BuildNodeActor->CheckIfTurretEmpty())
		{
			ChangeActorMaterial(BlueMaterial);
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, MouseHitResult.GetActor()->GetName());
		}
	}
	else
	{
		ChangeActorMaterial(RedMaterial);
	}
}

