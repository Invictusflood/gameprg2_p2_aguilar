// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowBuff.h"
#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"

USlowBuff::USlowBuff()
{

}

void USlowBuff::SetSlowSpeed(float NewSpeed)
{
	SlowSpeed = NewSpeed;
}

void USlowBuff::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("BurnBuff Was Created!"));

	ApplyBuff();

	//BuffEffectValue = 5;
	//duration = 5;

	//TimerDel.BindUFunction(this, FName("TriggerBurnTimer"));
}

void USlowBuff::TriggerBurnTimer()
{
	if (BuffTarget)
	{
		if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
			enemy->TakeDamage(BuffEffectValue);

		GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 1.f, false);
	}
	else
		DestroyBuff();//Destroy buff if target has also been destroyed
}

void USlowBuff::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	//curTime += 1 * DeltaTime;

	//if (curTime >= duration)
	//	RemoveBuff();
}

void USlowBuff::SetTarget(AActor* target)
{
	Super::SetTarget(target);
}

void USlowBuff::ApplyBuff()
{
	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
	{
		/*TriggerBurnTimer();*/
		enemy->GetCharacterMovement()->MaxWalkSpeed = (enemy->GetCharacterMovement()->MaxWalkSpeed * SlowSpeed);
		curTime = 0;
	}
}

void USlowBuff::DestroyBuff()
{
	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
	{
		enemy->GetCharacterMovement()->MaxWalkSpeed = (enemy->GetCharacterMovement()->MaxWalkSpeed / SlowSpeed);
	}

	Super::DestroyBuff();
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Buff Destroyed"));
}

void USlowBuff::SetDamagePerSec(int damage)
{
	BuffEffectValue = damage;
}